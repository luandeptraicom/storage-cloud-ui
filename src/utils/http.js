import axios from 'axios'
export const http = axios.create({
	baseURL: 'https://storage-cloud-balancer.cfapps.io',
  timeout: 5000,
  headers: {}
});
