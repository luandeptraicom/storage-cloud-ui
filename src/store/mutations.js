export default {
	SET_DOCUMENTS (state, files) {
		let fKey = k => k.id;
		state.files = files.reduce((a,c)=> (a[fKey(c)]=c,a),{})
	},
	VIEW_DOCUMENT (state, fileId) {
		state.currentFileView = state.files[fileId];
	},
	ADD_FILES (state, files) {
		if (files) {
			let fKey = k => k.id
			let f = files.reduce((a,c)=> (a[fKey(c)]=c,a))
			state.files = {...state.files, f}
		}
	},
	TOGGLE_POPUP_UPLOAD (state) {
		state.dialogUpload = !state.dialogUpload
	},
	TOGGLE_POPUP_VIEW (state) {
		state.dialogView = !state.dialogView
	}

}
