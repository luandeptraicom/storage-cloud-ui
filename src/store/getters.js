export default {
	files: state => {
		for (let item in state.files) {
			state.files[item].url = `https://storage-cloud-balancer.cfapps.io/${state.files[item].id}`;
		}
		return state.files;
	},
	currentFileView: state => state.currentFileView,
	pagination: state => state.pagination,
	dialogUpload: state => state.dialogUpload,
	dialogView: state => state.dialogView,
	dzUploadOptions: state => state.dzUploadOptions
}
