import {http} from '../utils/http'

export default {
	fetchDocuments ({commit}) {
		http.get('/files?p=0&s=1000')
			.then(r => r.data)
			.then(data => {
				if (data && data.meta.code == 200) {
					commit('SET_DOCUMENTS', data.data.files)
				}
			});
	},
	addFiles ({commit}, files) {
		commit('ADD_FILES', files);
	},
	viewDocument({commit}, fileId) {
		commit('VIEW_DOCUMENT', fileId)
	},
	togglePopupUpload ({commit}) {
		commit('TOGGLE_POPUP_UPLOAD')
	},
	togglePopupView ({commit}) {
		commit('TOGGLE_POPUP_VIEW')
	}
}
