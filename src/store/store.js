import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import getters from './getters'
import mutations from './mutations'
import actions from './actions'

export default new Vuex.Store({
	state: {
		files: [],
		currentFileView: {},
		currentPage: 0,
		currentSize: 20,
		dialogView: false,
		dialogUpload: false,
		dzUploadOptions: {
			url: 'https://storage-cloud-balancer.cfapps.io/files',
			paramName: 'files',
			//uploadMultiple: true,
			//chunking: true,
			//parrallelChunkUploads: true
		}
	},
	getters,
	mutations,
	actions
})
