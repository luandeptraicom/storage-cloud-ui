import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import Vuetify from 'vuetify'
import VueScroller from 'vue-scroller'
// index.js or main.js
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'viewerjs/dist/viewer.css'


Vue.use(Vuetify)
Vue.use(VueScroller)
Vue.config.productionTip = false

new Vue({
	store,
  render: h => h(App),
}).$mount('#app')
